<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 06.02.2018
 * Time: 21:15
 */

session_start();
setcookie('_antispam', 'enabled', time() + 3600);

require '../vendor/autoload.php';

$configFile = '../config/config.php';
$envFile = '../config/env.php';

use ES\Classes\App;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

if (file_exists($configFile) && file_exists($envFile)) {
  $config = include_once $configFile;
  $env = include_once $envFile;

  $logger = new Logger('site-log');
  $logger->pushHandler(new StreamHandler($env['debug_config']['log_path'], $env['debug_config']['debug_level']));

  $app = new App($config, $env, $logger); ?>

  <!doctype html>
  <html class="no-js" lang="">
  <head>
    <?php $app->_head(); ?>
  </head>

  <body>
  <?php $app->_header(); ?>

  <?php $app->_loadTemplate(); ?>

  <?php $app->_footer(); ?>
  </body>
  </html>

<?php } else {
  if (!file_exists($configFile)) {
    throw new Exception('no config file found');
  }
  if (!file_exists($envFile)) {
    throw new Exception('no env file found');
  }
}