<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 26.02.2018
 * Time: 21:52
 */

return [
  'default_language' => 'pl',
  'languages' => [
    'pl' => [
      'route' => '/',
      'culture' => 'pl-PL',
      'short' => 'pl',
      'label' => 'pl'
    ]
  ],
  'paths' => [
    '_home' => [
      'template' => 'home.php',
      'title' => [
        'pl' => ''
      ],
      'routes' => [
        'pl' => ''
      ]
    ],
    'house' => [
      'template' => 'house.php',
      'title' => [
        'pl' => 'Ty i dom'
      ],
      'routes' => [
        'pl' => 'ty-i-dom'
      ]
    ],
    'business' => [
      'template' => 'business.php',
      'title' => [
        'pl' => 'Ty i biznes'
      ],
      'routes' => [
        'pl' => 'ty-i-biznes'
      ]
    ],
    'certificates' => [
      'template' => 'certificates.php',
      'title' => [
        'pl' => 'Certyfikaty'
      ],
      'routes' => [
        'pl' => 'certyfikaty'
      ]
    ],
    'contact' => [
      'template' => 'contact.php',
      'title' => [
        'pl' => 'Kontakt'
      ],
      'routes' => [
        'pl' => 'kontakt'
      ]
    ],
    '404' => [
      'template' => '404.php',
      'title' => [
        'pl' => 'Nie znaleziono'
      ],
      'routes' => [
        'pl' => null
      ]
    ]
  ]
];