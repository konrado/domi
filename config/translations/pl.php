<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.03.2018
 * Time: 21:18
 */

return [
  'about' => 'o nas',
  'tariff' => 'cennik',
  'realizations' => 'realizacje',
  'demo' => 'demo i cennik',
  'login' => 'zaloguj się',
  'heading1' => 'Słuchamy głosu krowy',
  'heading2' => 'system monitorowania stada krów mlecznych',
  'subheading1_1' => 'Co to jest ',
  'subheading1_2' => 'e-stado?',
  'estado' => 'e-stado',
  'par1' => ' to system monitorowania stada krów mlecznych.<br> Zawiera funkcje monitoringu zdrowia zwierząt, rui, inseminacji i&nbsp;wycielenia. Jest bezinwazyjne i bezpieczne w użyciu, oparte o bezobsługowy czujnik montowany na uchu zwierzęcia.',
  'par2' => ' dostarcza informacji o stanie każdego zwierzęcia i&nbsp;całego stada 24 godziny na dobę, 7 dni w tygodniu.<br> Na bieżąco przesyła informacje do Ciebie lub doradcy hodowlanego, czy lekarza weterynarii.',
  'subheading2' => 'jak działa ',
  'how_it_works1_1' => 'Instalujemy urządzenia monitorujące Twoje<br>stado',
  'how_it_works1_2' => 'Czujniki są bezinwazyjne i&nbsp;bezpieczne w użyciu',
  'how_it_works2_1' => 'System e-stado monitoruje twoje stado i&nbsp;środowisko obory przez 24h',
  'how_it_works2_2' => 'System zbiera, analizuje dane i&nbsp;generuje wnioski',
  'how_it_works3_1' => 'Otrzymujesz bieżące informacje o stanie Twojego stada i&nbsp;środowiska obory.',
  'how_it_works3_2' => 'Otrzymumje dostęp do aplikacji www i&nbsp;powiadomienia SMS o nagłych zdarzeniach.',
  'subheading3' => 'poznaj zalety systemu ',
  'adv1_1' => 'Poczuj swobodę',
  'adv1_2' => 'Monitoruj swoje stado z dowolnego miejsca również poprzez SMS',
  'adv1_3' => 'Dzięki e-stado możesz monitorować stado przez komputer, tablet lub smartfon.',
  'adv2_1' => 'Kontroluj inseminację',
  'adv2_2' => 'Wykrywanie rui, określanie czasu inseminacji oraz historia wykrytych rui.',
  'adv2_3' => 'Dzięki e-stado skrócisz okres międzywycieleniowy z 440 dni do.',
  'adv3_1' => 'Monitoruj stan zdrowia stada',
  'adv3_2' => 'Monitoruj stan zdrowia, temperaturę, prawidłowość czasu przeżuwania.',
  'adv3_3' => 'Dzięki monitorowaniu stanu zdrowia i&nbsp;żywienia zmniejszysz koszty leczenia i&nbsp;brakowania w stadzie.',
  'adv4_1' => 'Zwiększ bezpieczeństwo wycielenia',
  'adv4_2' => 'Monitoring nadchodzącego wycielenia',
  'adv4_3' => 'Dzięki e-stado dowiesz się z wyprzedzeniem o czasie wycielenia.',
  'subheading4_1' => 'nasi ',
  'subheading4_2' => 'partnerzy ',
  'contact_heading' => 'Umów się<br> na spotkanie<br> i&nbsp;poznaj szczegóły',
  'contact_subheading' => 'Masz pytania?<br> Chciałbyś wdrożyć nasze rozwiązanie w swojej hodowli? Zostaw nam e-mail lub telefon. Skontaktujemy się z Tobą.',
  'success_msg' => 'Dziękujemy za przesłanie wiadomości',
  'error_msg' => '<span>Nie udało się wysłać wiadomości</span>',
  'field_req' => 'pole wymagane',
  'name' => 'imię',
  'phone' => 'telefon',
  'email' => 'email',
  'wrong_phone' => 'niepoprawny nr telefonu',
  'wrong_email' => 'niepoprawny adres email',
  'clause1' => 'Prosimy o zaznaczenie pola i&nbsp;tym samym wyrażenie zgody na przetwarzanie przez MICROLABS Sp. z o.o. podanych powyżej danych osobowych do celów marketingowych zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych (tekst jednolity: Dz. U. 2002 r. Nr 101 poz. 926 z późn. zm.)',
  'clause2' => 'Prosimy o zaznaczenie pola i&nbsp;tym samym wyrażenie zgody na otrzymywanie od MICROLABS Sp. z o.o. informacji handlowych drogą elektroniczną zgodnie z ustawą z dnia 18.07.2002 r. (Dz.U. nr 144, poz.1204 z późn. zm.) o świadczeniu usług drogą elektroniczną.',
  'clause3' => 'Administratorem danych osobowych jest MICROLABS Sp. z o.o., Al.Zwycięstwa 96/98,
      81-451 Gdynia<br>
      Podanie danych jest dobrowolne.<br>
      Dane osobowe będą przetwarzane wyłącznie w celu przedstawienia oferty systemu e-stado.<br>
      Dane osobowe będą przechowywane przez okres 24 miesięcy.<br>
      Dane osobowe nie będą udostępniane innym odbiorcom danych.<br>
      Osoba, której dane dotyczą ma prawo dostępu do treści swoich danych, ich sprostowania, usunięcia, ograniczenia
      przetwarzania, do sprzeciwu wobec przetwarzania, do przenoszenia danych, do cofnięcia zgody na przetwarzanie
      danych w dowolnym momencie poprzez kontakt: biuro@e-stado.net<br>
      Osoba, której dane dotyczą ma prawo do wniesienia skargi do organu nadzorczego: GIODO, ul. Stawki 2, 00-193
      Warszawa',
  'footer_title'=> 'Twój doradca',
  'phone2' => 'tel.',
  'about2' => 'O <span class="text-green">nas</span>',
  'par3' => 'Jesteśmy polską firmą. Jesteśmy producentem sprzętu do monitoringu zwier hodowlanych. Naszym celem jest wdrażanie najnowszych osiągnięć technologii elektronicznych i&nbsp;informatycznych do celów diagnostycznych. Stworzyliśmy system monitoringu krów mlecznych e-stado z myślą o potrzebach i&nbsp;możliwościach finansowych polskiego hodowcy.',
  'contact' => 'Kontakt',
  'tariff2' => 'System <span class="text-green">e-stado</span> dostępny jest<br>w&nbsp;dwóch pakietach',
  'price1_title' => 'Ruja + zdrowie',
  'price2_title' => 'Ruja + zdrowie + wycielenie',
  'func1' => 'Wykrywanie rui i&nbsp;określanie czasu inseminacji',
  'func2' => 'Historia wykrytych rui',
  'func3' => 'Ciągły pomiar temperatury ciała',
  'func4' => 'Monitoring prawidłowości czasu przeżuwania',
  'func5' => 'Alarmy poprzez SMS: ruja, inseminacja, gorączka, za krótki czas przeżuwania',
  'func6' => 'Monitoring nadchodzącego wycielenia',
  'func7' => 'Alarmy poprzez SMS: ruja, inseminacja, gorączka, za krótki czas przeżuwania, wycielenie',
  'price1' => 'netto / miesiąc od każdej krowy',
  'price2' => 'cena nie zawiera kosztów instalacji i&nbsp;sprzętu',
  'contact2' => 'skontaktuj się',
  'demo2' => 'uruchom demo aplikacji',
  'realizations2' => 'Realizacje'
];