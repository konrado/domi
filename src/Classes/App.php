<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.03.2018
 * Time: 21:31
 */

namespace ES\Classes;

use Monolog\Logger;

class App
{
  protected $config;
  protected $env;
  protected $languages;
  protected $currentLang;
  protected $pathLoader;
  protected $translator;
  protected $form;
  protected $logger;

  public function __construct(array $config, array $env, Logger $logger)
  {
    $this->config = $config;
    $this->env = $env;
    $this->logger = $logger;
    if (array_key_exists('languages', $config)) {
      $this->languages = $config['languages'];
      $this->currentLang = $config['languages'][$config['default_language']];
    }
    $parts = array();
    if (isset($_GET['path'])) {
      $parts = explode('/', $_GET['path']);
      if (count($parts)) {
        if (array_key_exists($parts[0], $this->languages)) {
          $this->currentLang = $this->languages[$parts[0]];
          $parts = array_slice($parts, 1);
        }
      }
    }

    $this->pathLoader = new PathLoader($config, $this->languages, $this->currentLang, $parts);
    $this->translator = new Translator($config, $this->currentLang);
    if(array_key_exists('mail', $this->env)) {
      $this->form = new Form($_POST, $this->env['mail'], $this->logger);
    }
  }

  public function envVar(string $name)
  {
    if (array_key_exists($name, $this->env)) {
      return $this->env[$name];
    }
    return null;
  }

  public function getLanguage()
  {
    return $this->currentLang;
  }

  public function paths(): PathLoader
  {
    return $this->pathLoader;
  }

  public function trans(): Translator
  {
    return $this->translator;
  }

  public function form(): Form
  {
    return $this->form;
  }

  public function loadTemplate()
  {
    $templateFile = $this->pathLoader->loadTemplate();
    ob_start();
    include $templateFile;
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
  }

  public function _loadTemplate()
  {
    echo $this->loadTemplate();
  }

  public function loadPart(string $partName)
  {
    $partFile = $this->pathLoader->loadPart($partName);
    ob_start();
    include $partFile;
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
  }

  public function _loadPart(string $partName)
  {
    echo $this->loadPart($partName);
  }

  public function head()
  {
    return $this->loadPart('head');
  }

  public function _head()
  {
    echo $this->head();
  }

  public function header()
  {
    return $this->loadPart('header');
  }

  public function _header()
  {
    echo $this->header();
  }

  public function footer()
  {
    return $this->loadPart('footer');
  }

  public function _footer()
  {
    echo $this->footer();
  }
}

