<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 10.03.2018
 * Time: 23:29
 */

namespace ES\Classes;

use DateTime;
use DateTimeZone;
use Monolog\Logger;
use PHPMailer\PHPMailer\PHPMailer;

class Form
{
  protected $postData;
  protected $mailConfig;
  protected $logger;
  protected $sendingResult;
  protected $errors;
  protected $posted;

  public function __construct(array $postData, array $mailConfig, Logger $logger)
  {
    $this->postData = $postData;
    $this->mailConfig = $mailConfig;
    $this->logger = $logger;
    $this->posted = !empty($postData);
    if ($this->posted) {
      $this->validateForm();
    }
  }

  public function validateForm()
  {
    $c = isset($_COOKIE['_antispam']) && $_COOKIE['_antispam'] == 'enabled';

    ob_start();
    var_dump($_SERVER);
    $server = ob_get_clean();
    ob_start();
    var_dump($_COOKIE);
    $cookie = ob_get_clean();
    $d = new DateTime();
    $d->setTimezone(new DateTimeZone('Europe/Warsaw'));
    $log = $d->format('Y-m-d H:i:s T')
      . "\r\nName:\t" . htmlspecialchars($_POST['name'])
      . "\r\nEmail:\t" . htmlspecialchars($_POST['phone'])
      . "\r\nPostcode:\t" . htmlspecialchars($_POST['email'])
      . "\r\nBogus1:\t" . htmlspecialchars($_POST['subject'])
      . "\r\nBogus2:\t" . htmlspecialchars($_POST['surname'])
      . "\r\nREMOTE_ADDR:\t" . (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '')
      . "\r\nHTTP_X_FORWARDED_FOR:\t" . (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '')
      . "\r\nCOOKIE:\t" . $c
      . "\r\nSERVER:\t" . $server
      . "\r\nSERVER:\t" . $cookie
      . "\r\n\r\n";
    $this->logger->addInfo('form: ' . $log);

    if (
      $c &&
      $_POST['subject'] === '' &&
      $_POST['surname'] === ''
    ) {
      if ($_POST['name'] !== '') {

      } else {
        $this->errors['name']['empty'] = true;
      }

      if ($_POST['phone'] !== '') {
        $phonePattern = '/^[0-9]{9,11}$/';
        $phoneMatch = preg_match($phonePattern, preg_replace('/[ +]/', '', htmlspecialchars($_POST['phone'])));
        if ($phoneMatch === 0) {
          $this->errors['phone']['format'] = true;
        }
      } else {
        $this->errors['phone']['empty'] = true;
      }

      if ($_POST['email'] !== '') {
        $emailPattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
        $emailMatch = preg_match($emailPattern, htmlspecialchars($_POST['email']));
        if ($emailMatch === 0) {
          $this->errors['email']['format'] = true;
        }
      }

      if (!count($this->errors)) {
        $subject = 'e-stado - formularz ze strony';
        $msg = '<p>Imię:' . htmlspecialchars($_POST['name']) . '</p>' .
          '<p>Telefon: ' . htmlspecialchars($_POST['phone']) . '</p>' .
          '<p>Email:' . htmlspecialchars($_POST['email']) . '</p>' .
          '<p>IP:' . $_SERVER['REMOTE_ADDR'] . '</p>' .
          '<p>Session id:' . session_id() . '</p>';
        return $this->sendMail($subject, $msg);
      } else {

        return false;
      }
    }

    return false;
  }

  protected function sendMail(string $subject, string $msg, string $address = null): bool
  {
    $mail = new PHPMailer;
    $mail->isSMTP();

    $mail->SMTPDebug = $this->mailConfig['smtpdebug'];

    $mail->Host = $this->mailConfig['host'];
    $mail->Port = $this->mailConfig['port'];
    $mail->SMTPSecure = $this->mailConfig['smtpsecure'];
    $mail->SMTPAuth = $this->mailConfig['smtpauth'];
    $mail->Username = $this->mailConfig['username'];
    $mail->Password = $this->mailConfig['password'];
    $mail->CharSet = $this->mailConfig['charset'];

    if ($address) {
      $mail->addAddress($address);
      foreach ($this->mailConfig['addbcc'] as $addr) {
        $mail->addBCC($addr);
      }
    } else {
      foreach ($this->mailConfig['addaddress'] as $addr) {
        $mail->addAddress($addr);
      }
    }
    $mail->setFrom($this->mailConfig['setfrom'][0], $this->mailConfig['setfrom'][1]);
    $mail->addReplyTo($this->mailConfig['addreplyto'][0], $this->mailConfig['addreplyto'][1]);
    $mail->Subject = $subject;
    $mail->msgHTML($msg);

    $this->sendingResult = $mail->send();

    $d = new DateTime();
    $d->setTimezone(new DateTimeZone('Europe/Warsaw'));
    $log = $d->format('Y-m-d H:i:s T')
      . "\r\nsuccess:\t" . $this->sendingResult
      . "\r\nsubject:\t" . $subject
      . "\r\naddress:\t" . $address
      . "\r\nmsg:\t" . $msg
      . "\r\n\r\n";
    $this->logger->addInfo('mail: ' . $log);

    return $this->sendingResult;
  }

  public function posted(): bool
  {
    return $this->posted;
  }

  public function getErrors(): array
  {
    return $this->errors ?: array();
  }

  public function sendingSuccessful(): bool
  {
    return $this->sendingResult ?: false;
  }
}