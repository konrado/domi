<div id="logo-waypoint" class="logo-wrapper">
  <a href="/"><img src="/img/logo_domi.svg" alt="domi logo"></a>
</div>

<main class="home">
  <div class="top-wrapper">
    <div class="top-image"></div>
    <h1>Tu dzięki nowym technologiom<br>kiełkują rozwiązania dla Ciebie</h1>
    <div class="mouse">
      <div></div>
    </div>
  </div>
  <div class="waypoint red-background">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <a href="<?php echo $this->paths()->getPath('house'); ?>" class="button red with-anim l--fleft">Ty
          i&nbsp;dom</a>
        <a href="<?php echo $this->paths()->getPath('business'); ?>" class="button red with-anim delayed-2 l--fright">Ty
          i&nbsp;biznes</a>
      </div>
    </div>
  </div>
  <div class="waypoint content-with-image">
    <div class="wrapper">
      <div class="image with-anim" style="background-image: url('/img/home2.jpg');"></div>
      <div class="text with-anim">
        <div>
          <a href="<?php echo $this->paths()->getPath('certificates'); ?>"
             class="button-small text-center">Certyfikaty</a>
          <h2>O&nbsp;nas</h2>
          <p>DOM-I jest integratorem systemów, zajmujemy się najnowszymi rozwiązaniami w zakresie automatyki,
            teletechniki i multimediów.
          </p>
          <p>Stanowimy zespół wykwalifikowanych inżynierów i specjalistów, pasjonatów nowych technologii. Jesteśmy
            otwarci na wiedzę, uczestniczymy nieustannie w szkoleniach, zdobyliśmy bogate doświadczenie przy wielu
            zrealizowanych inwestycjach.
          </p>
          <p>Oferujemy kompleksowe usługi w procesie inwestycyjnym, doradztwo i projektowanie, wykonawstwo instalacji
            oraz dostawę urządzeń. Zajmujemy się profesjonalnym wdrożeniem systemów, szkoleniem użytkowników a także
            obsługą
            serwisową.
          </p>
          <p>Jako integrator system automatyki, posiadamy w portfolio duże światowe standardy jak również systemy
            producenckie. Jesteśmy Partnerem KNX – światowego standardu automatyzacji dla domów i budynków a także
            członkiem stowarzyszenia KNX Polska.
          </p>
          <p>Na podstawie pozytywnej oceny Rady Naukowej PPNT i Ekspertów PPNT firma DOM-I została przyjęta w poczet
            partnerów Pomorskiego Parku Naukowo-Technologicznego <a href="http://www.ppnt.pl">www.ppnt.pl</a>.</p>
          <div class="logos-wrapper">
            <img src="/img/knx_logo.png">
            <img src="/img/ppnt_logo.png">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <h3 class="text-white with-anim with-margin">10 LAT DOŚWIADCZENIA</h3>
        <h4 class="text-white with-anim delayed-2">PONAD 100 REALIZACJI PRYWATNYCH I&nbsp;KOMERCYJNYCH</h4>
        <a href="<?php echo $this->paths()->getPath('contact'); ?>"
           class="button-small red with-anim delayed-6">Kontakt</a>
      </div>
    </div>
  </div>
</main>