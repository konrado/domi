<div id="logo-waypoint" class="logo-wrapper">
  <a href="/"><img src="/img/logo_domi.svg" alt="domi logo"></a>
</div>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->envVar('google_maps_api_key'); ?>"></script>

<script type="text/javascript">
  // When the window has finished loading create our google map below
  google.maps.event.addDomListener(window, 'load', init);

  function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
      // How zoomed in you want the map to start at (always required)
      zoom: 17,

      // The latitude and longitude to center the map (always required)
      center: new google.maps.LatLng(54.492547, 18.539676), // New York

      // How you would like to style the map.
      // This is where you would paste any style found on Snazzy Maps.
      styles: [{
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
      }, {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
      }, {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
      }, {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
      }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
      }, {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{"color": "#dedede"}, {"lightness": 21}]
      }, {
        "elementType": "labels.text.stroke",
        "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
      }, {
        "elementType": "labels.text.fill",
        "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
      }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
      }]
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    var icon = {
      url: '/img/pin_domi.png', // url
      size: new google.maps.Size(40, 40), // scaled size
      //origin: new google.maps.Point(50, 50), // origin
      anchor: new google.maps.Point(20, 20) // anchor
    };

    // Let's also add a marker while we're at it
    new google.maps.Marker({
      position: new google.maps.LatLng(54.492547, 18.539826),
      map: map,
      title: 'DOMI-I',
      icon: icon
    });
  }
</script>

<main class="contact">
  <div class="top-wrapper">
    <div class="va-middle">
      <div>
        <div class="links">
          <a href="tel:+48587270538">+48 58 727 05 38</a><br>
          <a href="mailto:biuro@dom-i.pl">BIURO@DOM-I.PL</a>
        </div>
        <p>al. Zwycięstwa 96/98/A106<br>
          81–451 Gdynia</p>
        <div class="clause">
          DOM-I&nbsp;Sp. z o.o.&nbsp;Sp.&nbsp;k.&nbsp;z&nbsp;siedzibą w&nbsp;Gdyni,<br> al.&nbsp;Zwycięstwa&nbsp;96/98/A106,&nbsp;81–451
          Gdynia,<br> wpisaną do rejestru
          przedsiębiorców Krajowego Rejestru Sądowego prowadzonego przez Sąd Rejonowy Gdańsk — Północ VIII Wydział
          Gospodarczy<br> KRS&nbsp;pod&nbsp;nr&nbsp;0000601927,<br> NIP&nbsp;5862303770,<br> REGON&nbsp;363730214.
        </div>
      </div>
    </div>
    <div class="mouse">
      <div></div>
    </div>
  </div>
  <div id="map"></div>
</main>

