<div id="logo-waypoint" class="logo-wrapper">
  <a href="/"><img src="/img/logo_domi.svg" alt="domi logo"></a>
</div>

<main class="house">
  <div class="top-wrapper">
    <div class="top-image" style="background-image: url('/img/house1.jpg')"></div>
    <h1>Kompleksowo zajmiemy się<br>rozwiązaniami dla Twojego domu.</h1>
    <div class="mouse">
      <div></div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <img src="/img/dom_inteligentny.svg" class="icon with-anim">
        <h3 class="text-white with-anim delayed-4">DOM INTELIGENTNY</h3>
      </div>
    </div>
  </div>
  <div class="waypoint content-with-image">
    <div class="wrapper">
      <div class="image with-anim" style="background-image: url('/img/house2.jpg')"></div>
      <div class="text with-anim">
        <div>
          <p>Najnowocześniejsze technologie dla budynków zapewniające bezpieczeństwo, komfort i oszczędności stały się
            standardem.</p>
          <p>Dzisiejsze projekty oświetlenia potrzebują mądrego rozwiązania, aby zapewnić domownikom prostą regulację i
            intuicyjną obsługę.</p>
          <p>System inteligentny ekonomicznie dopasuje ogrzewanie, klimatyzacje i wentylacje do panujących warunków.</p>
          <p>Rolety zamkną się, gdy nadejdzie wieczór, markiza złoży się przy silnym wietrze, a stacja pogodowa
            zdecyduje, kiedy uruchomić podgrzewanie rynien.</p>
          <p>Zbudujmy dom, który myśli – dom inteligentny.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <img src="/img/bezpieczenstwo.svg" class="icon with-anim">
        <h3 class="text-white with-anim delayed-4">BEZPIECZEŃSTWO</h3>
      </div>
    </div>
  </div>
  <div class="waypoint content-with-image">
    <div class="wrapper">
      <div class="image with-anim" style="background-image: url('/img/house3.jpg')"></div>
      <div class="text with-anim">
        <div>
          <p>Każdy z nas wie jak ważne jest poczucie bezpieczeństwa we własnym domu.</p>
          <p>System włamaniowy to jeden z najbardziej skutecznych sposobów zapewnienia spokoju rodzinie oraz chronienia
            naszego dobytku.</p>
          <p>Rozwiązania z zakresu kontroli dostępu – brelok, palec, nie potrzebujemy już kluczy, które łatwo
            zgubić.</p>
          <p>Kamery pracujące w dzień i w nocy, podgląd zdalny, przy furtce wideodomofon, aby dzwonek nie był
            niewiadomą.</p>
          <p>To właśnie jest bezpieczeństwo.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <img src="/img/rozrywka.svg" class="icon with-anim">
        <h3 class="text-white with-anim delayed-4">ROZRYWKA</h3>
      </div>
    </div>
  </div>
  <div class="waypoint content-with-image">
    <div class="wrapper">
      <div class="image with-anim" style="background-image: url('/img/house4.jpg')"></div>
      <div class="text with-anim">
        <div>
          <p>Jak cenny jest czas, kiedy możemy odpocząć, zrelaksować się, cieszyć się domem.</p>
          <p>System domowej rozrywki to dźwięk i obraz, na pewno usłyszysz wysoką jakość muzyki i zauważysz różnicę
            oglądania filmu, koncertu, meczu na dużym ekranie.</p>
          <p>Kompleksowe nagłośnienie każdego z pomieszczeń, gdzie każdy z domowników będzie mógł słuchać ulubionej
            muzyki.</p>
          <p>Odpowiednia infrastruktura, aby mieć swobodę wyboru dowolnej platformy, dowolnego dostawcy treści.</p>
          <p>Stwórz swoje prywatne centrum rozrywki.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <h3 class="text-white with-anim with-margin">10 LAT DOŚWIADCZENIA</h3>
        <h4 class="text-white with-anim delayed-2">PONAD 100 REALIZACJI PRYWATNYCH I&nbsp;KOMERCYJNYCH</h4>
        <a href="<?php echo $this->paths()->getPath('contact'); ?>"
           class="button-small red with-anim delayed-6">Kontakt</a>
      </div>
    </div>
  </div>
</main>
