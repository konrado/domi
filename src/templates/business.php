<div id="logo-waypoint" class="logo-wrapper">
  <a href="/"><img src="/img/logo_domi.svg" alt="domi logo"></a>
</div>

<main class="business">
  <div class="top-wrapper">
    <div class="top-image" style="background-image: url('/img/business1.jpg')"></div>
    <h1>Kompleksowo zajmiemy się<br>rozwiązaniami dla Twojego biznesu.</h1>
    <div class="mouse">
      <div></div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <img src="/img/automatyka_teletechnika_multimedia.svg" class="icon with-anim">
        <h3 class="text-white with-anim delayed-4">AUTOMATYKA / TELETECHNIKA / MULTIMEDIA</h3>
      </div>
    </div>
  </div>
  <div class="waypoint content-with-image">
    <div class="wrapper">
      <div class="image with-anim" style="background-image: url('/img/business2.jpg');"></div>
      <div class="text with-anim">
        <div>
          <p><span class="font-bold">AUTOMATYKA</span><br>
            System Zarządzania Budynkiem (BMS)</p>
          <p><span class="font-bold">TELETECHNIKA</span><br>
            System Sieci Strukturalnej<br>
            System Sieci Bezprzewodowej<br>
            System Sygnalizacji Pożaru (SSP)<br>
            Dźwiękowy System Ostrzegawczy (DSO)<br>
            System Oddymiania (SO)<br>
            System Sygnalizacji Włamania i&nbsp;Napadu (SSWiN)<br>
            System Kontroli Dostępu (SKD)<br>
            Rejestracja Czasu Pracy (RCP)<br>
            System Telewizji Przemysłowej (CCTV)<br>
            System Wideodomofonu<br>
            System Zamykania Drzwi</p>
          <p><span class="font-bold">MULTIMEDIA</span><br>
            System Audiowizualny<br>
            System Multiroom<br>
            System RTV/SAT</p>
        </div>
      </div>
    </div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <h3 class="text-white with-anim with-margin">10 LAT DOŚWIADCZENIA</h3>
        <h4 class="text-white with-anim delayed-2">PONAD 100 REALIZACJI PRYWATNYCH I&nbsp;KOMERCYJNYCH</h4>
        <a href="<?php echo $this->paths()->getPath('contact'); ?>"
           class="button-small red with-anim delayed-6">Kontakt</a>
      </div>
    </div>
  </div>
</main>