<div id="logo-waypoint" class="logo-wrapper">
  <a href="/"><img src="/img/logo_domi.svg" alt="domi logo"></a>
</div>

<main class="certificates">
  <div class="top-wrapper">
    <div class="top-image" style="background-image: url('/img/certificates1.jpg')"></div>
    <h1>Latami budowaliśmy swoje doświadczenie,<br>teraz możemy zaspokoić Twoje potrzeby.</h1>
    <div class="mouse"><div></div></div>
  </div>
  <div class="waypoint red-background wide">
    <div class="wrapper text-center va-middle">
      <div class="content">
        <img src="/img/certyfikaty.svg" class="icon with-anim">
        <h3 class="text-white with-anim delayed-4">CERTYFIKATY / REFERENCJE</h3>
      </div>
    </div>
  </div>
  <div class="dates">
    <div class="wrapper">
      <?php
      $content = file_get_contents('../config/certificates.txt');
      $parsed = [];
      $eol = substr_count($content, "\r\n") ? "\r\n" : "\n";
      foreach (explode($eol.$eol, $content) as $data) {
        $row_p = new stdClass();
        foreach (explode($eol, $data) as $row) {
          if (substr($row, 0, 4) === '    ') {
            $row_p->descr[] = substr($row, 4);
          } elseif (substr($row, 0, 1) === "\t") {
            $row_p->descr[] = substr($row, 1);
          } else {
            $row_p->date = $row;
          }
        }
        $parsed[] = $row_p;
      }
      ?>

      <?php foreach ($parsed as $p) : ?>
        <div class="date-wrapper">
          <h5><?php echo $p->date; ?></h5>
          <p>
            <?php foreach ($p->descr as $d) : ?>
              <?php echo $d; ?><br>
            <?php endforeach; ?>
          </p>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</main>