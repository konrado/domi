<div id="logo-waypoint" class="logo-wrapper">
  <a href="/"><img src="/img/logo_domi.svg" alt="domi logo"></a>
</div>

<main class="err404">
  <div class="top-wrapper">
    <div class="va-middle">
      <div class="text-center">
        <h6>404</h6>
        <h1>Nie znaleziono strony</h1>
        <a href="/" class="button-small">Powrót na stronę główną</a>
      </div>
    </div>
  </div>
</main>