<header>
  <div class="bar">
    <div class="wrapper">

    </div>
  </div>

  <nav id="menu">
    <ul class="bg-linear-green">
      <li><a href="<?php echo $this->paths()->getPath('house'); ?>">Ty i dom</a></li>
      <li><a href="<?php echo $this->paths()->getPath('business'); ?>">Ty i biznes</a></li>
      <li><a href="<?php echo $this->paths()->getPath('certificates'); ?>">Certyfikaty</a></li>
      <li><a href="<?php echo $this->paths()->getPath('contact'); ?>">Kontakt</a></li>
      <li class="back-nav" id="back1"><a href="#top"></a></li>
    </ul>
  </nav>

  <div class="hamb">
    <a id="top" href="#menu">
      <div>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </a>
  </div>

  <div id="contact-waypoint" class="contact-button">
    <div class="wrapper">
      <a href="<?php echo $this->paths()->getPath('contact'); ?>" class="button-small text-center">Kontakt</a>
    </div>
  </div>
</header>
