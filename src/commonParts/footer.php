<footer>
  <div class="text-center">
    <ul>
      <li>
        <a href="<?php echo $this->paths()->getPath('house'); ?>" <?php echo $this->paths()->getCurrentRoute() === 'house' ? 'class="active"' : ''; ?>>Ty
          i dom</a>
      </li>
      <li>
        <a href="<?php echo $this->paths()->getPath('business'); ?>" <?php echo $this->paths()->getCurrentRoute() === 'business' ? 'class="active"' : ''; ?>>Ty
          i biznes</a>
      </li>
      <li>
        <a href="<?php echo $this->paths()->getPath('certificates'); ?>" <?php echo $this->paths()->getCurrentRoute() === 'certificates' ? 'class="active"' : ''; ?>>Certyfikaty</a>
      </li>
      <li>
        <a href="<?php echo $this->paths()->getPath('contact'); ?>" <?php echo $this->paths()->getCurrentRoute() === 'contact' ? 'class="active"' : ''; ?>>Kontakt</a>
      </li>
    </ul>
    <a href="/">
      <div class="box"></div>
    </a>
    <p><a href="http://www.blowandflow.pl" target="_blank">made by Blow and Flow</a></p>
  </div>
</footer>
<script src="/js/main.js"></script>
