new Waypoint({
  element: document.getElementById('logo-waypoint'),
  handler: function(direction) {
    if(direction === 'down') {
        document.getElementsByTagName('header')[0].className += ' show';
        this.element.className += ' sticky';
    } else {
        document.getElementsByTagName('header')[0].className = document.getElementsByTagName('header')[0].className.replace(' show', '');
        this.element.className = this.element.className.replace(' sticky', '');
    }
  },
  offset: 10
});

new Waypoint({
  element: document.getElementById('contact-waypoint'),
  handler: function(direction) {
    if(direction === 'down') {
        this.element.className += ' sticky';
    } else {
        this.element.className = this.element.className.replace(' sticky', '');
    }
  },
  offset: 10
});

[].forEach.call(
  document.querySelectorAll('.waypoint'),
  function (el) {
    var w = new Waypoint({
      element: el,
      handler: function(direction) {
        w.destroy();
        this.element.className += ' animate';
      },
      offset: '80%'
    });
  }
);

//safari mobile highlight fix
document.addEventListener("touchstart", function(){}, true);